<?php

//Enable cross origin access control
header("Access-Control-Allow-Origin: *");

//Load recources
require_once('./classes/recordset.class.php');
// require_once('./classes/recordSet_rmv_header.class.php');
require_once('./classes/pdoDB.class.php');

//Start Session
session_start();

//Time and Date
$mysqlDateandTime = date("Y-m-d H:i:s");


//Standard REQUEST
$call = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'error';
$subject = isset($_REQUEST['subject']) ? $_REQUEST['subject'] : null;
$idPengguna = isset($_REQUEST['idPengguna']) ? $_REQUEST['idPengguna'] : null;
$namaPengguna = isset($_REQUEST['namaPengguna']) ? $_REQUEST['namaPengguna'] : null;
$usernamePengguna = isset($_REQUEST['usernamePengguna']) ? $_REQUEST['usernamePengguna'] : null;
$passwordPengguna = isset($_REQUEST['passwordPengguna']) ? $_REQUEST['passwordPengguna'] : null;
//Action and Subject to Route
$route = $call . ucfirst($subject);

// Connect to db
$db = pdoDB::getConnection();

//set the header to json because everything is returned in that format
header("Content-Type: application/json");

switch ($route) {
    case 'showAllUsers':
        $sqlSearch = "SELECT * FROM users";
        $rs = new JSONRecordSet();
        $retval = $rs->setRecord($sqlSearch, null, null);
        echo $retval;
        break;
    case 'addBook':
        $sqlSearch = "INSERT INTO pengguna (`namaPengguna`,`noKPPengguna`,`usernamePengguna`,`passwordPengguna`,`telefonPengguna`,`jenisPengguna`) VALUES (:bname,:bgenre)";
        $rs = new JSONRecordSet();
        $retval = $rs->setRecord($sqlSearch, null,
            array(
                ':bname'=>$bname,
                ':bgenre'=>$bgenre
                )
            );
        echo $retval;
        break;

    case 'login':
        $sqlSearch = "SELECT * FROM pengguna WHERE `usernamePengguna`=:usernamePengguna";
        $rs = new JSONRecordSet();
        $retval = $rs->getRecordSet($sqlSearch, null,
            array(
                ':usernamePengguna'=>$usernamePengguna
                )
            );
        echo $retval;
        break;

}//end of switch
?>