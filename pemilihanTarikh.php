<?php 

    session_start();

    if(!isset($_SESSION['user'])) {
        header("Location: cinemaHSY.php");
        die();
    }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "pawagam";
    $wayang = $_POST['wayang'];
    $wayang = explode("#", $wayang);

    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT idMT, DATE(tarikhMasaMT) AS tarikhMT, TIME_FORMAT(TIME(tarikhMasaMT), '%h:%i %p') AS masaMT, hargaMT FROM masa_tayangan WHERE idWayang = '$wayang[0]' ORDER BY UNIX_TIMESTAMP(tarikhMasaMT) ASC";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayResult = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayResult, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    $sql = "SELECT infoWayang FROM wayang WHERE idWayang = '$wayang[0]'";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayResult2 = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayResult2, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    $sql = "SELECT DATE(tarikhMasaMT) AS tarikh, TIME_FORMAT(TIME(tarikhMasaMT), '%h:%i %p') AS masa FROM masa_tayangan WHERE idWayang = '$wayang[0]' ORDER BY UNIX_TIMESTAMP(tarikhMasaMT)";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayResult3 = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayResult3, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    // echo print_r($arrayResult3, true);

    $sql = "SELECT DATE(tarikhMasaMT) AS tarikh FROM masa_tayangan WHERE idWayang = '$wayang[0]' ORDER BY UNIX_TIMESTAMP(tarikhMasaMT)";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayResult4 = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayResult4, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    $conn->close();

?>

<!DOCTYPE html>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/pemilihanTarikh.css">


    <head>

        <title>Cinema HSY</title>

    </head>

    <body>

        <div class="container-fluid">

            <div class="row" id="header">

                <div class="col-md-12" id="nama" style="background-color:black;">

                    <a href="<?php echo $_SESSION['jenis']; ?>.php"><img id="logo" src="cinemaHSY.png"></a>

                </div>

            </div>

            <div class="row" id="line"><div class="col-md-12"></div></div>

            <div class="row">

                <div id="sidebar">

                    <center>

                        <img id="user" src="./user_icon.png">
                        <h4 style="margin:20px;font-weight:800">Pengguna: <?php echo ($_SESSION['username']); ?></h4>

                    <center>

                    <div id="navigationMenu">

                        <ul id="navbar">

                            <li><a href="pekerja.php" class="">Menu Utama</a></li>
                            <li><a href="pilihanPendaftaran.php" class="">Pendaftaran Pengguna Baru</li>   
                            <ul>
                                <li><a href="pendaftaran.php" class="">Masukkan Data</li>
                                <li><a href="muatnaikCSV.php" class="">Muat Naik Fail CSV</li>
                            </ul>
                            <li><a href="penempahan.php" class="active">Penempahan Tiket</a></li>

                        </ul>   

                    </div>

                    <a role="button" id="logoutButton" href="logout.php">Log Keluar</a>

                </div>   

                <div id="main-body" style="width:80%">

                    <div class="container" id="tiket">

                        <div class="row" id="title">

                            <h3> Pemilihan Tarikh </h3>

                        </div>

                        <div id="pilihan">

                            <div class="row pilihTarikh">

                                <div id="imej">

                                    <img class="portrait" src="<?php echo $wayang[2]; ?>" style="">

                                </div>

                                <div id="maklumat" style="width:55vw;">

                                    <h4 class="namaWayang" style="margin:20px;font-weight:lighter;"><span style="font-weight:bold;font-size:45px;margin-right: 20px;"><?php echo $wayang[1] . "</span>" . $wayang[3] . " minit"; ?></h4>
                                    <h5 style="margin:40px 20px;font-weight:lighter;"><?php echo $arrayResult2[0]['infoWayang']; ?> </h5>

                                </div>

                            </div>

                            <div class="row tarikhMT">

                                <h3 class="pilihHeading">Pilih Tarikh Tayangan:</h3></br>

                                <form action="pemilihanMasa.php" method="POST">

                                    <?php

                                        $newArray = array_unique($arrayResult4, SORT_REGULAR);
                                        $arrayMasa = [];
                                        $x = 0;
                                        $n = 0;
                                        $arrayTemp[] = array();
                                        $arrayMT = array(); 
                                        $infoWayang = $arrayResult2[0]['infoWayang'];

                                        while ($n < count($arrayResult3)) {
                                            if ($arrayResult3[$n]['tarikh'] == $newArray[$x]['tarikh']) {
                                                $arrayTemp[$x] = $arrayResult3[$n]['masa'];
                                                array_push($arrayMasa, $arrayTemp[$x]); 
                                                $n += 1;
                                            } elseif ($arrayResult3[$n]['tarikh'] != $newArray[$x]['tarikh']) {
                                                $x += 1;
                                                if ($arrayResult3[$n]['tarikh'] == $newArray[$x]['tarikh']) {
                                                    $arrayTemp[$x] = $arrayResult3[$n]['masa'];
                                                    array_push($arrayMasa, $arrayTemp[$x]); 
                                                    $n += 1;
                                                } else {
                                                    array_push($arrayMasa, $arrayTemp[$x]);
                                                    $n += 1;
                                                }
                                            } else {
                                                array_push($arrayMasa, $arrayTemp[$x]);
                                            }
                                        }
                                        
                                        if (count($arrayResult4) == count($arrayMasa)) {
                                            for ($x = 0; $x < count($arrayResult4); $x ++) {
                                                $temp = array();
                                                $temp[0] = $arrayResult4[$x]['tarikh'];
                                                $temp[1] = $arrayMasa[$x];
                                                array_push($arrayMT, $temp);
                                            }
                                        }

                                        $tempNum = 0;

                                        for ($counter = 0; $counter < count($arrayMT); $counter ++) {

                                            $tempTarikh = $newArray[$tempNum]['tarikh'];

                                            if ($counter == 0) {

                                                $arrayMT[$counter][0] = explode("-", $arrayMT[$counter][0]);

                                                switch ($arrayMT[$counter][0][1]) {
                                                    case "01":
                                                        $bulan = "Jan";
                                                    break;

                                                    case "02":
                                                        $bulan = "Feb";
                                                    break;

                                                    case "03":
                                                        $bulan = "Mac";
                                                    break;

                                                    case "04":
                                                        $bulan = "Apr";
                                                    break;

                                                    case "05":
                                                        $bulan = "Mei";
                                                    break;

                                                    case "06":
                                                        $bulan = "Jun";
                                                    break;

                                                    case "07":
                                                        $bulan = "Jul";
                                                    break;

                                                    case "08":
                                                        $bulan = "Ogo";
                                                    break;

                                                    case "09":
                                                        $bulan = "Sep";
                                                    break;

                                                    case "10":
                                                        $bulan = "Okt";
                                                    break;

                                                    case "11":
                                                        $bulan = "Nov";
                                                    break;

                                                    case "12":
                                                        $bulan = "Dis";
                                                    break;
                                                }


                                                echo ('<input type="hidden" name="urlGambar" value="');
                                                echo print_r($wayang[2], true);
                                                echo ('">');
                                                echo ('<input type="hidden" name="wayang" value="');
                                                echo $wayang[0] . "#" . $wayang[1];
                                                echo ('">');
                                                echo ('<input type="hidden" name="tempoh" value="');
                                                echo print_r($wayang[3], true);
                                                echo ('">');
                                                echo ('<input type="hidden" name="infoWayang" value="');
                                                echo htmlspecialchars(json_encode($infoWayang));
                                                echo ('">');
                                                echo ('<button class="tarikh" type="submit" name="tarikhMT" value="');
                                                echo implode("-", $arrayMT[$counter][0]);
                                                echo ('">');
                                                echo $arrayMT[$counter][0][2] . "</br>" . $bulan;
                                                echo ('</button>');

                                            } elseif ($arrayMT[$counter][0] == $newArray[$tempNum]['tarikh']) {

                                            } else {

                                                $arrayMT[$counter][0] = explode("-", $arrayMT[$counter][0]);

                                                switch ($arrayMT[$counter][0][1]) {
                                                    case "01":
                                                        $bulan = "Jan";
                                                    break;

                                                    case "02":
                                                        $bulan = "Feb";
                                                    break;

                                                    case "03":
                                                        $bulan = "Mac";
                                                    break;

                                                    case "04":
                                                        $bulan = "Apr";
                                                    break;

                                                    case "05":
                                                        $bulan = "Mei";
                                                    break;

                                                    case "06":
                                                        $bulan = "Jun";
                                                    break;

                                                    case "07":
                                                        $bulan = "Jul";
                                                    break;

                                                    case "08":
                                                        $bulan = "Ogo";
                                                    break;

                                                    case "09":
                                                        $bulan = "Sep";
                                                    break;

                                                    case "10":
                                                        $bulan = "Okt";
                                                    break;

                                                    case "11":
                                                        $bulan = "Nov";
                                                    break;

                                                    case "12":
                                                        $bulan = "Dis";
                                                    break;
                                                }

                                                echo ('<input type="hidden" name="urlGambar" value="');
                                                echo print_r($wayang[2], true);
                                                echo ('">');
                                                echo ('<input type="hidden" name="wayang" value="');
                                                echo $wayang[0] . "#" . $wayang[1];
                                                echo ('">');
                                                echo ('<input type="hidden" name="tempoh" value="');
                                                echo print_r($wayang[3], true);
                                                echo ('">');
                                                echo ('<input type="hidden" name="infoWayang" value="');
                                                echo htmlspecialchars(json_encode($infoWayang));
                                                echo ('">');
                                                echo ('<button class="tarikh" type="submit" name="tarikhMT" value="');
                                                echo implode("-", $arrayMT[$counter][0]);
                                                echo ('">');
                                                echo $arrayMT[$counter][0][2] . "</br>" . $bulan;
                                                echo ('</button>');

                                                $tempNum += 1;

                                            }
                                        }
                                    ?>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </body>