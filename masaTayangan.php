<?php 

    session_start();

    if(!isset($_SESSION['user'])) {
        header("Location: cinemaHSY.php");
        die();
    }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "pawagam";

    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT idWayang, namaWayang FROM wayang";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayResult = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayResult, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    $sql = "SELECT idBilik, noBilik FROM bilik";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayResult2 = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayResult2, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    $conn->close();

?>

<!DOCTYPE html>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/masaTayangan.css">


    <head>

        <title>Cinema HSY</title>

    </head>

    <body>

        <div class="container-fluid">

            <div class="row" id="header">

                <div class="col-md-12" id="nama" style="background-color:black;">

                    <a href="<?php echo $_SESSION['jenis']; ?>.php"><img id="logo" src="cinemaHSY.png"></a>

                </div>

            </div>

            <div class="row" id="line"><div class="col-md-12"></div></div>

            <div class="row">

                <div id="sidebar">

                    <center>

                        <img id="user" src="./user_icon.png">
                        <h4 style="margin:20px;font-weight:800">Pengguna: <?php echo ($_SESSION['username']); ?></h4>

                    <center>

                    <div id="navigationMenu">

                        <ul id="navbar">

                            <li><a href="pengurus.php" class="">Menu Utama</a></li>
                            <li><a href="pilihanPendaftaran.php" class="">Pendaftaran Pengguna Baru</li>   
                            <ul>
                                <li><a href="pendaftaran.php" class="">Masukkan Data</li>
                                <li><a href="muatnaikCSV.php" class="">Muat Naik Fail CSV</li>
                            </ul>
                            <li><a href="penempahan.php" class="active">Penempahan Tiket</a></li>

                        </ul>   

                    </div>

                    <a role="button" id="logoutButton" href="logout.php">Log Keluar</a>

                </div>   

                <div id="main-body" style="width:80%">

                    <div class="container" id="movies">

                        <div class="row" id="title">

                            <h3> Penentuan Masa Tayangan </h3>

                        </div>

                        <form action="mtValidation.php" method="POST">

                            <h3 class="label">Sila pilih nama wayang: </h3>

                            <select id="namaWayang" class="namaWayang" name="movie" required>

                                <?php

                                    for ($x = 0; $x < count($arrayResult); $x ++){

                                        echo ('<option value="');
                                        echo print_r($arrayResult[$x]['idWayang'], true);
                                        echo ('">');
                                        echo print_r($arrayResult[$x]['idWayang'], true) . ". ";
                                        echo print_r($arrayResult[$x]['namaWayang'], true);
                                        echo ('</option>');

                                    }

                                ?>
                            
                            </select>
                        
                            <h3 class="label">Sila pilih tarikh tayangan: </h3>

                            <input type="date" name="tarikh" class="tarikhTayangan" required>

                            <h3 class="label">Sila pilih masa tayangan: </h3>

                            <input type="time" name="masa" class="masaTayangan" required>

                            <h3 class="label">Sila pilih bilik: </h3>

                            <select id="bilik" class="bilik" name="bilik" required>

                                <?php

                                    for ($y = 0; $y < count($arrayResult2); $y ++){

                                        echo ('<option value="');
                                        echo print_r($arrayResult2[$y]['idBilik'], true);
                                        echo ('">');
                                        echo print_r($arrayResult2[$y]['idBilik'], true) . ". ";
                                        echo "Bilik " . print_r($arrayResult2[$y]['noBilik'], true);
                                        echo ('</option>');

                                    }

                                ?>
                            
                            </select>

                            <h3 class="label">Sila masukkan harga: </h3>

                            <input type="number" name="harga" placeholder="1.0" step="0.1" min="5" max="25">

                            <button class="masukMT" type="submit" style="font-size:20px;">Masukkan Masa Tayangan</button>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </body>

</html>

                            

