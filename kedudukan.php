<?php 

    session_start();

    if(!isset($_SESSION['user'])) {
        header("Location: cinemaHSY.php");
        die();
    }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "pawagam";
    $url = $_POST['urlGambar'];
    $wayang = explode("#", $_POST['wayang']);
    $tempoh = $_POST['tempoh'];
    $masa = $_POST['masaMT'];
    $tarikh = $_POST['tarikh'];
    $tarikh1 = explode("#", $_POST['tarikh']);
    $bulan = $tarikh1[1];
    $tarikhPerkataan = explode("-", $tarikh1[0]);
    $tarikhMT = $tarikh1[0];

    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT idMT, idBilik FROM masa_tayangan WHERE idWayang = '$wayang[0]' AND DATE(tarikhMasaMT) = '$tarikh' AND TIME_FORMAT(TIME(tarikhMasaMT), '%h:%i %p') = '$masa'";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $array = [];
        while ($row = $result->fetch_assoc()) {
            array_push($array, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }

    $idBilik = $array[0]['idBilik'];

    $sql = "SELECT noBilik, saizBilik FROM bilik WHERE idBilik = '$idBilik'";
    $result = $conn->query($sql);

    if ($conn->query($sql) == TRUE){
        $arrayBilik = [];
        while ($row = $result->fetch_assoc()) {
            array_push($arrayBilik, $row);
        }
    } else {
        echo "Error" . $sql . "<br>" . $conn->error;
    }
    
    $conn->close();

    echo 'hello';

?>

<!DOCTYPE html>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/kedudukan.css">


    <head>

        <title>Cinema HSY</title>

    </head>

    <body>

        <div class="container-fluid">

            <div class="row" id="header">

                <div class="col-md-12" id="nama" style="background-color:black;">

                    <a href="<?php echo $_SESSION['jenis']; ?>.php"><img id="logo" src="cinemaHSY.png"></a>

                </div>

            </div>

            <div class="row" id="line"><div class="col-md-12"></div></div>

            <div class="row">

                <div id="sidebar">

                    <center>

                        <img id="user" src="./user_icon.png">
                        <h4 style="margin:20px;font-weight:800">Pengguna: <?php echo ($_SESSION['username']); ?></h4>

                    <center>

                    <div id="navigationMenu">

                        <ul id="navbar">

                            <li><a href="pekerja.php" class="">Menu Utama</a></li>
                            <li><a href="pilihanPendaftaran.php" class="">Pendaftaran Pengguna Baru</li>   
                            <ul>
                                <li><a href="pendaftaran.php" class="">Masukkan Data</li>
                                <li><a href="muatnaikCSV.php" class="">Muat Naik Fail CSV</li>
                            </ul>
                            <li><a href="penempahan.php" class="active">Penempahan Tiket</a></li>

                        </ul>   

                    </div>

                    <a role="button" id="logoutButton" href="logout.php">Log Keluar</a>

                </div>   

                <div id="main-body" style="width:80%">
                
                    <div class="kedudukan" style="width:100%">

                        <div class="container" style="width:100%;text-align:center;">

                            <div class="row desc" style="width:100%;text-align:center;display:block;">

                                <h4><?php echo $wayang[1] . "<span> | </span>" . $tempoh . "minit" . "<span> | </span>" . $tarikhPerkataan[2] . " " . $bulan . " " . $tarikhPerkataan[0] . "<span> | </span>" . $masa . "<span> | </span>" . $arrayBilik[0]['noBilik']; ?></h4> 

                            </div>

                        </div>

                        <form action="penempahanValidation.php" method="POST">

                            <div class="row screen">
                                
                                <h5>Screen</h5>

                            </div>

                            <?php

                                echo '<div class="pelanBilik">';
                                echo '<table class="pelan">';


                                if ($arrayBilik[0]['saizBilik'] == 140) {

                                    $rowLetter = array("A", "B", "D", "E", "F", "G", "H", "I", "J");

                                    for ($row = 0; $row < count($rowLetter); $row ++) {
                                        echo '<tr>';
                                        for ($seat = 1; $seat < 15; $seat ++) {
                                            if ($seat == 2 || $seat == 12) {
                                                echo '<td>';
                                                echo '<input type="checkbox" id="';
                                                echo $rowLetter[$row] . sprintf('%02d', $seat);
                                                echo '" name="seat[]">';
                                                echo '<label for="';
                                                echo $rowLetter[$row] . sprintf('%02d', $seat);
                                                echo '">';
                                                echo $rowLetter[$row] . sprintf('%02d', $seat);
                                                echo '</label>';
                                                echo '</td>';
                                                echo '<td></td>';
                                            } else {
                                                echo '<td>';
                                                echo '<input type="checkbox" id="';
                                                echo $rowLetter[$row] . sprintf('%02d', $seat);
                                                echo '" name="seat[]">';
                                                echo '<label for="';
                                                echo $rowLetter[$row] . sprintf('%02d', $seat);
                                                echo '">';
                                                echo $rowLetter[$row] . sprintf('%02d', $seat);
                                                echo '</label>';
                                                echo '</td>';
                                            }
                                        }
                                    }

                                }

                            ?>

                        </form>

                    </div>
                
                </div>

                    