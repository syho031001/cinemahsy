<?php

    session_start();
    date_default_timezone_set("Asia/Kuala_Lumpur");

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "pawagam";
    $idWayang = $_POST['movie'];
    $tarikhTayangan = explode("-", $_POST['tarikh']);
    $masaTayangan = str_replace(":", "", $_POST['masa']);
    $tarikhKini = explode("-", date("Y-m-d"));
    $masaKini = str_replace(":", "", date("H:i"));
    $bilik = $_POST['bilik'];
    $harga = $_POST['harga'];
    $counter = 1;

    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($tarikhTayangan[0] > $tarikhKini[0]) {
        $validation = true;
        $x = 1;
    } elseif ($tarikhTayangan[0] == $tarikhKini[0]) {
        if ($tarikhTayangan[1] > $tarikhKini[1]) {
            $validation = true;
            $x = 1;
        } elseif ($tarikhTayangan[1] == $tarikhKini[1]) {
            if ($tarikhTayangan[2] > $tarikhKini[2]) {
                $validation = true;
                $x = 1;
            } elseif ($tarikhTayangan[2] == $tarikhKini[2]) {
                if ($masaTayangan > $masaKini) {
                    $validation = true;
                    $x = 1;
                } else {
                    $validation = false;
                    $x = 0;
                }
            } else {
                $validation = false;
                $x = 0;
            }
        } else {
            $validation = false;
            $x = 0;
        }
    } else {
        $validation = false;
        $x = 0;
    }

    if($x == $counter) {
        if ($validation === true) {
            $tarikhTayangan = $_POST['tarikh'];
            $masaTayangan = $_POST['masa'] . ":00";
            $tarikhMasa = $tarikhTayangan . " " . $masaTayangan;
    
            $sql = "INSERT INTO masa_tayangan (tarikhmasaMT, idBilik, idWayang, hargaMT) VALUES ('$tarikhMasa','$bilik','$idWayang','$harga')";
    
            $result = $conn->query($sql);

            $pass = true;
            
        } else {
            $pass = false;       
        }
    } else {
        $pass = false;
    }

    if ($pass == true) {
        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('Anda berjaya masukkan masa tayangan.')
        window.location.href='./masaTayangan.php';
        </SCRIPT>");
        $conn->close();         
    } else {
        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('Anda gagal masukkan masa tayangan. Sila cuba lagi.')
        window.location.href='./masaTayangan.php';
        </SCRIPT>");
        $conn->close();
    }

?>