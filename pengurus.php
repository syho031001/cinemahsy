<?php 

session_start();

if(!isset($_SESSION['user'])) {
    header("Location: cinemaHSY.php");
    die();
}

?>

<!DOCTYPE html>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <head>

        <style>

            html {
                width: 100%;
                height: 100%;
                overflow-x: hidden;
            }

            body {
                font-family: "Candara", "Courier New";
                width: 100%;
                height: 100%;
                overflow-x: hidden;
            }

            #header {
                position: sticky;
                top: 0;
                z-index: 99;
                font-family: "Courier New";
                height: 88px;
            }

            #logoutButton {
                border: 0px;
                font-size: 18px;
                border-radius: 10px;
                background-color: black;
                color: orange;
                width: 150px;   
                height: 60px;
                line-height: 40px;
                padding: 10px;
                margin: 30px;
            }

            #logo {
                margin-left: auto;
                margin-right: auto;
                height: 36px;
                width: auto;
                z-index: 99;
                display: block;
                margin-top: 26px;
            }

            #line {
                position: sticky;
                top: 88px;
                z-index: 99;
                background-color: orange;
                height: 8px;
                
            }

            #user {
                width: 50px;
                height: auto;
                margin-top: 20px;
            }

            #sidebar {
                width: 20%;
                height: 100%;
                background-color: orange;
                position: fixed;
                overflow-x: hidden;
                z-index: 99;
            }

            #navigationMenu {
                margin-top: 40px;
                text-align: left;
            }

            #navigationMenu a {
                margin: 20px;
                margin-top: 40px;
                font-size: 18px;
                color: black;
            }

            .active {
                font-weight: bold;
                text-transform: uppercase;
                border-radius: 10px;
            }
            
            #main-body {    
                margin-left: 20%;
                width: 80%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.7);
            }

            .container-fluid {
                background-color: rgba(0, 0, 0, 0.7);
            }

            #slideshow {
                height: 430px;
            }

            .slidershow {
                width: 700px;
                height: 360px;
                overflow: hidden;
            }

            .middle {
                margin-left: 90%;
                margin-top: 31%;
                transform: translate(-50%,-50%);
            }

            .navigation {
                position: absolute;
                bottom: 20px;
                left: 50%;
                transform: translateX(-50%);
                display: flex;
            }

            .bar {
                width: 50px;
                height: 10px;
                border: 2px solid #fff;
                opacity: 0.85;
                margin: 6px;
                cursor: pointer;
                transition: 0.4s;
            }

            .bar:hover {
                background: #fff;
            }

            .bar:visited {
                color: white;
            }

            input[name="r"] {
                position: absolute;
                visibility: hidden;
                background-color: white;
            }

            .slides {
                width: 3500px;
                height: 100%;
                display: flex;  
            }

            .slide {
                width: 700px;
                height: 100%;
                transition: 0.6s;
            }

            .slide img {
                width: 700px;
                height: auto;
                position: relative;
                bottom: 0;
            }

            #r2:checked ~ .s1 {
                margin-left: -20%;
            }

            #r3:checked ~ .s1 {
                margin-left: -40%;
            }

            #r4:checked ~ .s1 {
                margin-left: -60%;
            }

            #r5:checked ~ .s1 {
                margin-left: -80%;
            }

        </style>

        <title>Cinema HSY</title>

    </head>

    <body>

        <div class="container-fluid">

            <div class="row" id="header">

                <div class="col-md-12" id="nama" style="background-color:black;">

                    <a href="pekerja.php"><img id="logo" src="cinemaHSY.png"></a>

                </div>

            </div>

            <div class="row" id="line"><div class="col-md-12"></div></div>

            <div class="row">

                <div id="sidebar">

                    <center>

                        <img id="user" src="./user_icon.png">
                        <h4 style="margin:20px;font-weight:800">Pengguna: <?php echo ($_SESSION['username']); ?></h4>

                    <center>

                    <div id="navigationMenu">

                        <ul id="navbar">

                            <li><a href="pekerja.php">Menu Utama</a></li>
                            <li><a href="pendaftaran.php">Pendaftaran Pengguna Baru</a></li>
                            <li><a href="penempahan.php">Penempahan Tiket</a></li>

                        </ul>

                    </div>

                    <a role="button" id="logoutButton" href="logout.php">Log Keluar</a>

                </div>

                <div id="main-body" style="width:80%">

                    <div class="row" style="width:80%;">

                        <center id="slideshow">

                            <div class="slidershow middle">

                                <div class="slides">

                                    <input type="radio" name="r" id="r1" checked>
                                    <input type="radio" name="r" id="r2">
                                    <input type="radio" name="r" id="r3">
                                    <input type="radio" name="r" id="r4">
                                    <input type="radio" name="r" id="r5">

                                    <div class="slide s1">

                                        <img src="ip_man.jpeg" alt="">

                                    </div>

                                    <div class="slide">

                                        <img src="blackWidow.jpg" alt="">

                                    </div>

                                    <div class="slide">

                                        <img src="jumanji.jpg" alt="">

                                    </div>

                                    <div class="slide">

                                        <img src="4.jpg" alt="">

                                    </div>

                                    <div class="slide">

                                        <img src="5.jpg" alt="">

                                    </div>

                                    <div class="navigation">

                                        <label for="r1" class="bar"></label>
                                        <label for="r2" class="bar"></label>
                                        <label for="r3" class="bar"></label>
                                        <label for="r4" class="bar"></label>
                                        <label for="r5" class="bar"></label>

                                    </div>

                                </div>

                            </div>

                        </center>
                    
                    </div>

                </div>

            </div>

        </div>

    </body>

</html>